# Základy umělé inteligence

This repository contains material for the [ZUI course](https://cw.fel.cvut.cz/b202/courses/b4b36zui/start)

See each lab directory for specific material.
